/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include "dipsw.h"
#include "lcd.h"
#include "rtc.h"
#include "par.h"
#include "pci.h"

/* ------------------------------------------------------------------------- */

/*
 * Check Board Identity:
 */

int checkboard (void)
{
	puts ("Board: Cogent " COGENT_MOTHERBOARD " motherboard with a "
	      COGENT_CPU_MODULE " CPU Module\n");
	return (0);
}

/* ------------------------------------------------------------------------- */

/*
 * Miscelaneous platform dependent initialisations while still
 * running in flash
 */

int misc_init_f (void)
{
	printf ("DIPSW: ");
	dipsw_init ();
	return (0);
}

/* ------------------------------------------------------------------------- */

phys_size_t initdram (int board_type)
{
#ifdef CONFIG_CMA111
	return (32L * 1024L * 1024L);
#else
	unsigned char dipsw_val;
	int dual, size0, size1;
	long int memsize;

	dipsw_val = dipsw_cooked ();

	dual = dipsw_val & 0x01;
	size0 = (dipsw_val & 0x08) >> 3;
	size1 = (dipsw_val & 0x04) >> 2;

	if (size0)
		if (size1)
			memsize = 16L * 1024L * 1024L;
		else
			memsize = 1L * 1024L * 1024L;
	else if (size1)
		memsize = 4L * 1024L * 1024L;
	else {
		printf ("[Illegal dip switch settings - assuming 16Mbyte SIMMs] ");
		memsize = 16L * 1024L * 1024L;	/* shouldn't happen - guess 16M */
	}

	if (dual)
		memsize *= 2L;

	return (memsize);
#endif
}

/* ------------------------------------------------------------------------- */

/*
 * Miscelaneous platform dependent initialisations after monitor
 * has been relocated into ram
 */

int misc_init_r (void)
{
	printf ("LCD:   ");
	lcd_init ();

#if 0
	printf ("RTC:   ");
	rtc_init ();

	printf ("PAR:   ");
	par_init ();

	printf ("KBM:   ");
	kbm_init ();

	printf ("PCI:   ");
	pci_init ();
#endif
	return (0);
}
